﻿using UnityEngine;
using System.Collections.Generic;

public class VOICE : MonoBehaviour {

    AudioSource asUISound;
    AudioSource asMusic;
    List<AudioSource> asSound = new List <AudioSource>();
    AudioSource[] audios;
    private int loopingAusIndex;
    public static VOICE me { get; private set; }
    private bool created = false;

    void Awake() {
        if (me != null && me != this) {
// First we check if there are any other instances conflicting
            Destroy(gameObject); // If that is the case, we destroy other instances
        } else {
            me = this; // Here we save our singleton instance
            GameObject.DontDestroyOnLoad(this.gameObject);
        }

        audios = GetComponents <AudioSource>();
        asMusic = audios[0];
        asUISound = audios[1];
        for (int i = 2; i < 5; ++i) {
            audios[i].loop = false;
            audios[i].volume = 1.0F;
            asSound.Add(audios[i]);
        }
        //HotFix for Unity3D volume bug isseu
        GetComponent <Transform>().position = Camera.main.GetComponent <Transform>().position;

    }
    

    public void PlayUI(AudioClip a) {
        asUISound.Stop();
        asUISound.clip = a;
        asUISound.Play();
    }

    public void MusicPlay(AudioClip a) {
        asMusic.clip = a;
        asMusic.loop = true;
        asMusic.Play();
    }

  

    public void MusicOFF() {
        PlayerPrefs.SetInt("MusicEnabled", 0);
        asMusic.volume = 0.0F;
    }

    public void MusicON() {
        PlayerPrefs.SetInt("MusicEnabled", 1);
        asMusic.volume = 1.0F;
    }

    public void SoundOFF() {
        PlayerPrefs.SetInt("SoundEnabled", 0);
        asUISound.volume = 0.0F;
        for (int i = 0; i < asSound.Count; ++i) {
            asSound[i].volume = 0.0F;
        }
    }

    public void SoundON() {
        PlayerPrefs.SetInt("SoundEnabled", 1);
        asUISound.volume = 1.0F;
        for (int i = 0; i < asSound.Count; ++i) {
            asSound[i].volume = 1.0F;
        }
    }

    public void MusicSetVolume(float volume) {
        asMusic.volume = volume;
    }

    public void MusicStop() {
        asMusic.Stop();
    }
    public void MusicPause() {
        asMusic.Pause();
    }

    public void MusicUnPause() {
        asMusic.UnPause();
    }

    private int TakeFreeAus() {
        for (int i = 0; i < 3; ++i) {
            if (!asSound[i].isPlaying)
                return i;
        }
        // Если все заняты - добавить еще один и вернуть его индекс
        AddFreeAud();
        return asSound.Count - 1;
    }

    private void AddFreeAud() {
         AudioSource newAudioSource =  gameObject.AddComponent<AudioSource>();
        newAudioSource.volume = 1.0f;
        newAudioSource.spatialBlend = 0;
        newAudioSource.loop = false;
        asSound.Add(newAudioSource);
    }
    


    public void PlaySound(AudioClip a ) {
        int freeAusIndex = TakeFreeAus();
        asSound[freeAusIndex].Stop();
        asSound[freeAusIndex].clip = a;
        asSound[freeAusIndex].Play();
    }

    public void PlaySoundWithVolume( AudioClip a, float v ) {
        int freeAusIndex = TakeFreeAus();
        float initVolume = asSound[freeAusIndex].volume;
        asSound[freeAusIndex].Stop();
        asSound[freeAusIndex].clip = a;
        asSound[freeAusIndex].volume = v;
        asSound[freeAusIndex].Play();
        asSound[freeAusIndex].volume = initVolume;
    }


}
