﻿using UnityEngine;
using System.Collections;

public class AbstractCanvasButton : MonoBehaviour {

    public AudioClip clipOnPress;

    // Use this for initialization
    public void bntPress() {
         StartCoroutine(bntPressCoorutine());
    }

    IEnumerator bntPressCoorutine()
    {
        VOICE.me.PlayUI(clipOnPress);
        yield return new WaitForSeconds(clipOnPress.length+0.1f);
        OnPressBehaviour();
    }

    public virtual void OnPressBehaviour() {
        Debug.LogFormat("Game object {0} used script {1} with unoverloaded method OnPressBehaviour()", gameObject.name, this.name);
    }
    
}
