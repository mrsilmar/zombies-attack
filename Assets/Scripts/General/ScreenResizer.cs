﻿using UnityEngine;
using System.Collections;

public class ScreenResizer : MonoBehaviour {
  

    private readonly float DEFAULT_WIDTH = 20.48F;
    private readonly float DEFAULT_HEIGHT = 15.36F;

    public static Transform GreatParent;
 

    void Awake() {
        GreatParent = GetComponent <Transform>();

        float _worldScreenHeight = Camera.main.orthographicSize * 2f;
        float  _worldScreenWidth = _worldScreenHeight / Screen.height * Screen.width;

        float scaleX = _worldScreenWidth / DEFAULT_WIDTH;
        float scaleY = _worldScreenHeight / DEFAULT_HEIGHT;

        GreatParent.localScale = new Vector3(scaleX, scaleY, 1);

    }

}