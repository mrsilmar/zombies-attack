﻿using UnityEngine;
using System.Collections;

public class btnStartGame : AbstractCanvasButton {
    public string SceneToLoad;
    public float MusicValueForGameScene = 0.8f;
    public override void OnPressBehaviour() {
        SceneLoader.me.ChangeScene(SceneToLoad);
        VOICE.me.MusicSetVolume(MusicValueForGameScene);
    }
}
