﻿using UnityEngine;
using System.Collections;

public class CameraSingleton : MonoBehaviour {

    public static CameraSingleton me { get; private set; }

    void Awake() {
        if (me != null && me != this) {
            Destroy(gameObject);
        } else {
            me = this;
            GameObject.DontDestroyOnLoad(this.gameObject);
        }

    }
}
