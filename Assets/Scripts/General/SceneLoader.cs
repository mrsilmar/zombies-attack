﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    public static SceneLoader me { get; private set; }

    void Awake() {
        if (me != null && me != this) {
            Destroy(gameObject); 
        } else {
            me = this; 
            GameObject.DontDestroyOnLoad(this.gameObject);
        }
    }

    public void ChangeScene(int id) {
        SceneManager.LoadScene(id);
    }

    public void ChangeScene(string name) {
        SceneManager.LoadScene(name);
    }
}
