﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BestScore : MonoBehaviour {

    private Text txtScore;

    void Start() {
        txtScore = GetComponent <Text>();
        if (PlayerPrefs.HasKey("BestScore")) {
            txtScore.text = String.Format("Best Score: {0}", PlayerPrefs.GetInt("BestScore"));
        }
    }
}
