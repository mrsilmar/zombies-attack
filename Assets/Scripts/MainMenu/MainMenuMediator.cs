﻿using UnityEngine;
using System.Collections;

public class MainMenuMediator : MonoBehaviour {

    public AudioClip acMusic;
    public float MusicVolumeOnMenu = 0.5f;

 
    void Start () {
        VOICE.me.MusicSetVolume(0.5f);
        VOICE.me.MusicPlay(acMusic);
    }
    
}
