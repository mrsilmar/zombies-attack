﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CurrentScoreScript : MonoBehaviour {

    private Text txtScore;

    void Start() {
        txtScore = GetComponent <Text>();
        txtScore.text =  txtScore.text = String.Format("Current Score: {0}", GameSceneMediator.me.points);

    }
}
