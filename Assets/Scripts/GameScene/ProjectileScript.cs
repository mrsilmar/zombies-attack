﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {

    private Rigidbody2D r2D;

    public float Damage;
    public GameObject Blow;
    public Vector2 MoveVector = new Vector2(0,1);
    public float Speed = 1;
    

    void Awake () {
        r2D = GetComponent <Rigidbody2D>();
    }


    public void UpdateVelosity() {
         r2D.velocity = MoveVector * Speed;
    }

}
