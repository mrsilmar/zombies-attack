﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    // Use this for initialization
     void OnCollisionEnter2D(Collision2D coll) {
         if (coll.gameObject.tag == "projectile") {
             coll.gameObject.Recycle();
         }

     }
}
