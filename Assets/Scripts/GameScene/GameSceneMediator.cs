﻿using System;
using UnityEngine;
using System.Collections;

public class GameSceneMediator : MonoBehaviour {

    public TextMesh tm;
    public GameObject goGameOver;
    public AudioClip acGameOver;
    public int points;
    public bool isGameOver;
    public static GameSceneMediator me  { get; private set; }
    // Use this for initialization

    void Awake() {
        me = this;
    }
    void Start() {
        isGameOver = false;
        points = 0;
        Messenger.AddListener<int>("AddPoints",AddPoints);
        Messenger.AddListener("GameOver",GameOver);
        Debug.Log(tm.gameObject.name);
    }

    void OnDestroy() {
      Messenger.RemoveListener <int>("AddPoints", AddPoints);
      Messenger.RemoveListener("GameOver", GameOver);
    }

    void AddPoints(int p) {
        points += p;
        tm.text = points.ToString();
    }

    void GameOver() {
        VOICE.me.MusicStop();
        VOICE.me.PlaySound(acGameOver);
        isGameOver = true;
        if (!PlayerPrefs.HasKey("BestScore")) {
            PlayerPrefs.SetInt("BestScore", points);
        } else {
            if (PlayerPrefs.GetInt("BestScore") < points) {
                PlayerPrefs.SetInt("BestScore", points);
            }
        }
        goGameOver.SetActive(true);
    }
    
 
}
