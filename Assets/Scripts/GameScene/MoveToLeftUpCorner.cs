﻿using UnityEngine;
using System.Collections;

public class MoveToLeftUpCorner : MonoBehaviour {

    public float marginX;
    public float marginY;

    // Use this for initialization
    void Start() {

        Vector2 CameraRigthTop = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
        Vector2 CameraLeftBot  = Camera.main.ScreenToWorldPoint(Vector2.zero);

        // float yCameraHeight = CameraRigthTop.y - CameraLeftBot.y;
        // float xCameraWidth  = CameraRigthTop.x - CameraLeftBot.x;

        transform.position = new Vector3(CameraLeftBot.x + marginX,CameraRigthTop.y - marginY,-5.0f);

    }


}
