﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class ZombieSpawn : MonoBehaviour {

    public float ZombieSpawnRate = 2.0f;
    public float ZombieTimeDecrement = 0.1f;
    public float ZombieTimePeriod = 10.0f;
    public float ZombieMinSpawnRate = 0.5f;

    public BoxCollider2D[] SpawnZones;

    public GameObject[] ZombiePrefabs;
    public float[] ZombieProbabilities;

    private float ZombieSpawnRateCurrent;
    private Vector3[] SpawnZonesGlobalCenter;



    void Start() {

        ZombieSpawnRateCurrent = ZombieSpawnRate;
        InvokeRepeating("DecreaseSpawnRate", 0.001f, ZombieTimePeriod);
        Array.Resize(ref SpawnZonesGlobalCenter, SpawnZones.Length);

        //кешируем центры зон спавная зомби
        for (int i = 0; i < SpawnZonesGlobalCenter.Length; ++i) {
            SpawnZonesGlobalCenter[i] = SpawnZones[i].transform.position;
        }

        Spawn();
    }

    void DecreaseSpawnRate() {
        if (ZombieSpawnRateCurrent > ZombieMinSpawnRate) {
            ZombieSpawnRateCurrent -= ZombieTimeDecrement;
        }
    }


    void Spawn() {
        if (GameSceneMediator.me.isGameOver) {
            StopAllCoroutines();
            return;
        }

        int SpawnZoneIndex = Random.Range(0, SpawnZones.Length);

        Vector3 ZombiePos = GetRandomPointInBox(SpawnZones[SpawnZoneIndex]);
        ZombiePos += SpawnZonesGlobalCenter[SpawnZoneIndex];

        int ZombieIndex = ChooseZombie(ZombieProbabilities);
        ZombiePrefabs[ZombieIndex].Spawn(ZombiePos);
        Invoke("Spawn", ZombieSpawnRateCurrent);
    }

    Vector2 GetRandomPointInBox(BoxCollider2D b2D) {
        return b2D.offset + new Vector2(
            (Random.value - 0.5f) * b2D.size.x,
            (Random.value - 0.5f) * b2D.size.y
            );

    }

    int ChooseZombie(float[] probs) {
        float total = 0;

        for (int i = 0; i < probs.Length; ++i) {
            total += probs[i];
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Length; ++i) {
            if (randomPoint < probs[i]) {
                return i;
            } else {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }


    // Use this for initialization

}
