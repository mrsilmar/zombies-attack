﻿using UnityEngine;

public class Zombie : MonoBehaviour {

    public AudioClip acSpawn;
    public AudioClip acDead;
    public int startHp;
    public float speed;
    public int zombiePoints;

    private BoxCollider2D bc;
    private Rigidbody2D r2D;
    private int hp;
    private bool recycled;
    private Transform trHero;
    private Transform trZombie;
    private Vector2 moveVector;
    private float rotationZ;
    private Transform hpBar;


    private void OnEnable() {
        if (!recycled) return;
        Prepare();
    }

    private void Start() {
        Messenger.AddListener("GameOver",GameOver);
        bc = GetComponent <BoxCollider2D>();
        r2D = GetComponent <Rigidbody2D>();
        trHero = GameObject.FindGameObjectWithTag("hero").GetComponent <Transform>();
        trZombie = GetComponent <Transform>();
        hpBar = trZombie.GetChild(0).GetChild(0);
        Prepare();
    }

    private void OnDestroy() {
        Messenger.RemoveListener("GameOver",GameOver);
    }

    private void Prepare() {
        VOICE.me.PlaySound(acSpawn);
        moveVector = trHero.position - trZombie.position;
        moveVector.Normalize();
        hp = startHp;
        r2D.velocity = moveVector * speed;
        rotationZ = Mathf.Atan2(moveVector.y, moveVector.x) * Mathf.Rad2Deg;
        trZombie.rotation = Quaternion.Euler(0f, 0f, rotationZ - 90);
        hpBar.localScale = Vector3.one;
        
    }

    

    private void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "projectile") {
            coll.gameObject.Recycle();
           
            Hit();
        }

    }

    protected virtual void GameOver() {
         r2D.velocity = Vector2.zero;
    }

    //can be overloaded in parents
    protected virtual void Hit() {
        hp -= 1;
        float xSize = 1.0f * hp / startHp;
        
        hpBar.localScale = new Vector3(xSize,1.0f,1.0f);
        if (hp == 0) {
            Die();
        }
    }

    protected virtual void Die() {
        Messenger.Broadcast("AddPoints",zombiePoints);
        VOICE.me.PlaySound(acDead);
        recycled = true;
        gameObject.Recycle();
    }

    
}
