﻿using UnityEngine;

public class HeroScript : MonoBehaviour {

    public float spawnPerSec = 10.0f;
    public int damage = 1;
    public GameObject projectile;


    private Vector2 p0; 
    private Vector2 moveVector = new Vector2(0, 1);
    private Vector2 moveVectorProjectile;
    private Vector2 p0_cam; 

    private Transform trHero;
    private Transform trGun;
    private Vector2 v2HeroPos;
    private float rotationZ = 90.0f;


    void Start() {
        trHero = GetComponent <Transform>();
        trGun = trHero.GetChild(0);
        v2HeroPos = new Vector2(trHero.position.x, trHero.position.y);
        FireGun();
    }


    void FireGun() {
        if (GameSceneMediator.me.isGameOver) return;

        GameObject proj = projectile.Spawn(trGun.position);
        proj.transform.rotation = trHero.rotation;

        ProjectileScript ps = proj.GetComponent <ProjectileScript>();
        ps.MoveVector = trHero.up;

        //чем больше прожектайлов в секунду - темы выше скорость
        ps.Speed = spawnPerSec;
        ps.UpdateVelosity();

        Invoke("FireGun", 1.0f / spawnPerSec);
    }

    void Update() {
        if ((Input.touchCount == 1)) {
            CalculateMoveVector();
            RotateHero();
        }
    }


    void CalculateMoveVector() {
        Touch touch = Input.touches[0];
        // Не стал делать тач фазы (нет событий на отпускание и начало движения).
        p0 = touch.position;
        p0_cam = Camera.main.ScreenToWorldPoint(p0);
        moveVector = p0_cam - v2HeroPos;
        moveVector.Normalize();
    }


    void RotateHero() {
        rotationZ = Mathf.Atan2(moveVector.y, moveVector.x) * Mathf.Rad2Deg;
        // Нельзя смотреть вниз
        if (rotationZ > 0) {
            trHero.rotation = Quaternion.Euler(0f, 0f, rotationZ - 90);
        }
    }


    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "zombie") {
            Messenger.Broadcast("GameOver");
        }

    }

}


